const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const path = require('path');

module.exports = {
  entry: ['@babel/polyfill', './src/index.js'],
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'js/bundle.js',
  },
  mode: 'none',
  devServer: {
    /*hot: true,
    open: true,
    contentBase: path.join(__dirname, "dist"),
    compress: true,
    port: 9001,*/
    port: 9182,
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
      },
      {
        test: /\.(jpeg|png|jpg|svg|gif)$/i,
        //use: 'file-loader',
        //en el caso de que la imagen sea mas pesada que 1000k,
        //utilizar file-loader, de otro modo converti la imagen
        //a base 64
        use: {
          loader: 'url-loader',
          options: {
            name: '[name].[hash:6].[ext]',
            outputPath: 'img',
            publicPath: 'img',
            emitFile: true,
            esModule: false,
            limit: 1000,
          },
        },
      },
      {
        test: /\.(scss|css)$/,
        exclude: /node_modules/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              reloadAll: true,
            },
          },
          'css-loader',
          'sass-loader',
        ],
      },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, 'public', 'index.html'),
    }),
    new MiniCssExtractPlugin({
      filename: 'css/bundle.css',
    }),
  ],
};
